const express = require("express");
const router = express.Router();
const Guest = require("../models/Guest");

router.post("/", async (req, res, next) => {
  const {
    _id,
    isattending,
    name,
    contact,
    email,
    address,
    dietaryrestrictions,
    requirements,
    parkingcoupon,
    additionalGuests,
  } = req.body;

  Guest.findById(_id)
    .then((guest) => {
      guest.isattending = isattending;
      guest.name = name;
      guest.contact = contact;
      guest.email = email;
      guest.address = address;
      guest.dietaryrestrictions = dietaryrestrictions;
      guest.requirements = requirements;
      guest.parkingcoupon = parkingcoupon;
      guest.additionalGuests = additionalGuests;
      return guest.save();
    })
    .then((result) => {
      console.log("Updated");
    })
    .catch((error) => {
      console.log(error);
    });
});

module.exports = router;
