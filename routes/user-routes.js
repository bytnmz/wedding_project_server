const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const userControllers = require("../controllers/user-controllers");

router.post(
  "/signup",
  [
    check("userid").not().isEmpty().isLength({ min: 6 }),
    check("password").not().isEmpty().isLength({ min: 8 }),
  ],
  userControllers.signup
);
router.post("/login", userControllers.login);

module.exports = router;
