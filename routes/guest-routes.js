const express = require("express");
const router = express.Router();
const guestControllers = require("../controllers/guest-controllers");
const auth = require("../middleware/auth");

router.patch("/update", guestControllers.updateGuest);
router.post("/get", guestControllers.getGuest);

router.use(auth);

router.get("/getAll", guestControllers.getAllGuest);
router.post("/add", guestControllers.addGuest);
router.delete("/remove", guestControllers.removeGuest);

module.exports = router;
