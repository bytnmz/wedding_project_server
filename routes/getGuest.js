const express = require("express");
const router = express.Router();
const Guest = require("../models/Guest");

router.post("/", async (req, res, next) => {
  const { id } = req.body;

  console.log(id);
  const guestData = await Guest.findById(id);

  res.send(guestData);
});

module.exports = router;
