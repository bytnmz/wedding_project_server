const express = require("express");
const router = express.Router();
const Guest = require("../models/Guest");
const sendEmail = require("../email/email");

router.post("/", async (req, res, next) => {
  const {
    isattending,
    name,
    contact,
    email,
    address,
    dietaryrestrictions,
    requirements,
    parkingcoupon,
    additionalGuests,
  } = req.body;

  const guest = new Guest({
    isattending,
    name,
    contact,
    email,
    address,
    dietaryrestrictions,
    requirements,
    parkingcoupon,
    additionalGuests,
  });

  // const emailMessage = {
  //   from: "bayu.tanmizi@gmail.com",
  //   to: "bytnmz@gmail.com",
  //   subject: "Using SendGrid SMTP",
  //   text: "Test content",
  //   html: "<strong>HTML content</strong>",
  // };

  // sendEmail(emailMessage).catch(console.error);

  guest
    .save()
    .then((result) => {
      console.log("Guest data saved");
    })
    .catch((error) => {
      console.log(error);
    });

  res.send("Okay");
});

module.exports = router;
