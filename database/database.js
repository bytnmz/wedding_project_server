const mongoose = require("mongoose");

async function connectToDB(app) {
  try {
    console.log("Connecting to database...");
    await mongoose.connect(
      `mongodb+srv://${process.env.DBUSERNAME}:${process.env.DBPASSWORD}@bayuandcayllaproject.skhkn.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`
    );

    console.log("Successfully connected to database...");
    app.listen(process.env.PORT || 8000);
  } catch (error) {
    console.log(error);
  }
}

module.exports = connectToDB;
