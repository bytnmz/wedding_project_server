const { rsort } = require("semver");
const Guest = require("../models/Guest");
const HttpError = require("../models/HttpError");
// const sendEmail = require("../email/email");

const addGuest = async (req, res, next) => {
  const {
    isattending,
    name,
    contact,
    email,
    address,
    dietaryrestrictions,
    requirements,
    parkingcoupon,
    additionalGuests,
  } = req.body;

  const newGuest = new Guest({
    isattending,
    name,
    contact,
    email,
    address,
    dietaryrestrictions,
    requirements,
    parkingcoupon,
    additionalGuests,
  });

  // const emailMessage = {
  //   from: "bayu.tanmizi@gmail.com",
  //   to: "bytnmz@gmail.com",
  //   subject: "Using SendGrid SMTP",
  //   text: "Test content",
  //   html: "<strong>HTML content</strong>",
  // };

  // sendEmail(emailMessage).catch(console.error);

  try {
    await newGuest.save();
  } catch (err) {
    const error = new HttpError("Adding of guest data failed.", 500);
    return next(error);
  }

  res.status(200).json({
    message: "Guest added",
    guest: newGuest.toObject({ getters: true }),
  });
};

const getAllGuest = async (req, res, next) => {
  let guestList;

  try {
    guestList = await Guest.find();
  } catch (err) {
    const error = new HttpError("Fetching of guest data failed.", 500);
    return next(error);
  }

  if (!guestList) {
    const error = new HttpError("No guests found.", 404);
    return next(error);
  }

  res.status(200).json({ guestList });
};

const getGuest = async (req, res, next) => {
  const { id } = req.body;

  let existingGuest;

  try {
    existingGuest = await Guest.findById(id);
  } catch (err) {
    const error = new HttpError("Fetching of guest data failed.", 500);
    return next(error);
  }

  if (!existingGuest) {
    const error = new HttpError("Guest not found.", 404);
    return next(error);
  }

  res.status(200).json({ guest: existingGuest.toObject({ getters: true }) });
};

const updateGuest = async (req, res, next) => {
  const {
    _id,
    isattending,
    name,
    contact,
    email,
    address,
    dietaryrestrictions,
    requirements,
    parkingcoupon,
    additionalGuests,
  } = req.body;

  let existingGuest;
  try {
    existingGuest = await Guest.findById(_id);
  } catch (err) {
    const error = new HttpError("Updating of guest data failed.", 500);
    return next(error);
  }

  if (!existingGuest) {
    const error = new HttpError("Guest not found.", 404);
    return next(error);
  }

  existingGuest.isattending = isattending;
  existingGuest.name = name;
  existingGuest.contact = contact;
  existingGuest.email = email;
  existingGuest.address = address;
  existingGuest.dietaryrestrictions = dietaryrestrictions;
  existingGuest.requirements = requirements;
  existingGuest.parkingcoupon = parkingcoupon;
  existingGuest.additionalGuests = additionalGuests;

  try {
    await existingGuest.save();
  } catch (err) {
    const error = new HttpError("Updating of guest data failed.", 500);
    return next(error);
  }

  res.status(200).json({ guest: existingGuest.toObject({ getters: true }) });
};

const removeGuest = async (req, res, next) => {
  const { id } = req.body;

  let existingGuest;

  try {
    existingGuest = await Guest.findById(id);
  } catch (err) {
    const error = new HttpError("Failed to remove guest data.", 500);
    return next(error);
  }

  try {
    await existingGuest.remove();
  } catch (err) {
    const error = new HttpError("Failed to remove guest data.", 500);
    return next(error);
  }

  res.status(200).json({ message: "Guest removed." });
};

exports.addGuest = addGuest;
exports.getAllGuest = getAllGuest;
exports.getGuest = getGuest;
exports.updateGuest = updateGuest;
exports.removeGuest = removeGuest;
