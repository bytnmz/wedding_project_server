const { validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const HttpError = require("../models/HttpError");

const signup = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs received. Please check your inputs.", 422)
    );
  }

  const { userid, password } = req.body;

  let existingUser;
  try {
    existingUser = await User.findOne({ userid: userid });
  } catch (err) {
    const error = new HttpError(
      "Signing up failed. Please try again later.",
      500
    );
    return next(error);
  }

  if (existingUser) {
    const error = new HttpError("This user already exist.", 422);
    return next(error);
  }

  let hashedPassword;
  try {
    hashedPassword = await bcrypt.hash(password, 12);
  } catch (err) {
    const error = new HttpError("User cannot be created.", 500);
    return next(error);
  }

  const newUser = new User({
    userid,
    password: hashedPassword,
  });

  try {
    await newUser.save();
    console.log("New user created!", newUser);
  } catch (err) {
    const error = new HttpError("Failed to create a new user.", 500);
    return next(error);
  }

  let token;
  try {
    token = jwt.sign({ userid: newUser.userid }, process.env.JWTSECRET, {
      expiresIn: "1h",
    });
  } catch (err) {
    const error = new HttpError("Failed to create a new user.", 500);
    return next(error);
  }

  res.status(201).json({ userId: newUser.id, token: token });
};

const login = async (req, res, next) => {
  const { userid, password } = req.body;

  let existingUser;
  try {
    existingUser = await User.findOne({ userid: userid });
  } catch (err) {
    const error = new HttpError("Log in failed. Please try again later.", 500);
    return next(error);
  }

  if (!existingUser) {
    const error = new HttpError("User not found.", 401);
    return next(error);
  }

  let isValidPassword = false;
  try {
    isValidPassword = await bcrypt.compare(password, existingUser.password);
  } catch (err) {
    const error = new HttpError("Log in failed. Please try again later.", 500);
    return next(error);
  }

  if (!isValidPassword) {
    const error = new HttpError(
      "Log in failed. Invalid credentials provided.",
      401
    );
    return next(error);
  }

  let token;
  try {
    token = jwt.sign({ userid: existingUser.userid }, process.env.JWTSECRET, {
      expiresIn: "1h",
    });
  } catch (err) {
    const error = new HttpError("Log in failed.", 500);
    return next(error);
  }

  res.json({ userId: existingUser.id, token });
};

exports.signup = signup;
exports.login = login;
