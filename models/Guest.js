const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const guestSchema = new Schema({
  isattending: { type: String, required: true },
  name: { type: String, required: true },
  contact: { type: String, required: false },
  email: { type: String, required: false },
  address: { type: String, required: false },
  dietaryrestrictions: {
    vegan: { value: { type: Boolean, required: false } },
    halal: { value: { type: Boolean, required: false } },
    nobeef: { value: { type: Boolean, required: false } },
    others: {
      enabled: { type: Boolean, required: false },
      value: { type: String, required: false },
    },
  },
  requirements: { type: String, required: false },
  parkingcoupon: { type: String, required: false },
  additionalGuests: [
    {
      name: { type: String, required: true },
      dietaryrestrictions: {
        vegan: { value: { type: Boolean, required: false } },
        halal: { value: { type: Boolean, required: false } },
        nobeef: { value: { type: Boolean, required: false } },
        others: {
          enabled: { type: Boolean, required: false },
          value: { type: String, required: false },
        },
      },
    },
  ],
});

module.exports = mongoose.model("Guest", guestSchema);
