const nodemailer = require("nodemailer");

async function sendEmail(messageBody) {
  const transporter = nodemailer.createTransport({
    host: "smtp.sendgrid.net",
    port: 465,
    secure: true,
    auth: {
      user: "apikey",
      pass: `${process.env.SENDGRID_SMTP_API_KEY}`,
    },
  });

  const info = await transporter.sendMail(messageBody);
  console.log("Email sent successfully - ", info.response);
}

module.exports = sendEmail;
